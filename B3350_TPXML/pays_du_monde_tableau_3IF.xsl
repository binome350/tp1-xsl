<?xml version = "1.0" encoding = "UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html"/><!--	On affiche le début de la page HTML-->
	<xsl:template match="/">
		<html>
			<head><title>Pays du monde </title></head>
			<body style="background-color:white;">
				<h1>Les pays du monde</h1>
				<h2> Mise en forme par le binôme B3350 : </h2>
				<br/> * Vincent FALCONIERI 
				<br/> * Perrine GARNIER
      			<br/>
				<xsl:apply-templates select="//metadonnees"/>
				<xsl:apply-templates select="countries"/>
			</body>
		</html>
		
	</xsl:template>
	
	<!--	On gère l'affichage avant le tableau-->
	<xsl:template match="metadonnees">
	
	<!--	On affiche les objectifs-->
		<p style="text-align:center; color:blue;">	Objectif : 
		<xsl:value-of select="objectif"/>
		</p>
		
	<!--	On affiche les regions du monde avec le nombre de pays-->
	<xsl:variable name="listRegion" select="//country[not(preceding::region = region)]/region"/>
		Continents (regions) : 
	<xsl:for-each select="$listRegion">
		<xsl:choose>
		<!--	Quand le nom de region est rempli -->
		<xsl:when test="not(current() = '')">
			<xsl:value-of select="current()"/>
		</xsl:when>
					
		<!--	Quand le nom de region est vide.  -->
		<xsl:otherwise>	
			Sans continent
		</xsl:otherwise>
		</xsl:choose>
	
		(<xsl:value-of select="count(//country[region=current()])"/> pays)
		<xsl:if test="not(position()=last())">
			<xsl:text>, </xsl:text>
		</xsl:if>
	</xsl:for-each>
	
	<!--	On affiche le pays avec le plus de frontière -->
	<br/>
	Le pays ayant le plus de voisins frontaliers est :
	<xsl:for-each select="//country">
    		<xsl:sort select="count(borders)" data-type="number" order="descending"/>
    		<xsl:if test="position() = 1">
    			<xsl:value-of select="./name/common"/>
    			 (<xsl:value-of select="count(borders)"/>)
    		</xsl:if>
  	</xsl:for-each>

		<hr/>
	</xsl:template>
	
	<!--	On gère l'affichage pour l'encadrement du tableau principal -->
	<xsl:template match="countries">
		<xsl:variable name="listRegion" select="//country[not(preceding::region = region)]/region"/><!--	On itère sur les régions-->
		<xsl:for-each select="$listRegion">
			<h3>
Pays du continent :
	

<xsl:value-of select="current()"/>

par sous-régions :</h3><!--On itère sur les sous regions êtant composantes de cette régionl -->
			<xsl:variable name="listsubRegion" select="//country[not(preceding::subregion = subregion) and region=current()]/subregion"/>
			<xsl:for-each select="$listsubRegion">
				<h4>
					<xsl:value-of select="current()"/>
				</h4>
				<table width="100%" align="center" border="3"><!--	<table border="3" width="800" align="center">-->
					<tr>
						<th>
		N°
		</th>
						<th>
		Nom
		</th>
						<th>
		Capitale
		</th>
						<th>
		Voisins
		</th>
						<th>
		Coordonnées
		</th>
						<th>
		Drapeau
		</th>
					</tr>
					<xsl:apply-templates select="//../country[subregion=current()]"/>
				</table>
			</xsl:for-each>
		</xsl:for-each>
	</xsl:template><!--	On gère l'affichage de chaque ligne pour chaque country-->
	<xsl:template match="country">
		<tr>
			<td>
				<xsl:value-of select="position()"/>
			</td>
			<td>
				<span style="color:green">
					<xsl:value-of select="name/common"/>
				</span>	( <xsl:value-of select="name/official"/>  )
			
			<xsl:if test="boolean(name/native_name[@lang='fra'])">
					<br/>
					<span style="color:brown;"> Nom français :
			<xsl:value-of select="name/native_name[@lang='fra']/official"/>
					</span>
				</xsl:if>
			</td>
			<td>
				<xsl:value-of select="capital"/>
			</td>
			<td>
				<xsl:apply-templates select="borders"/>
			</td>
			<td>
				<xsl:apply-templates select="latlng"/>
			</td>
			<td>
				<xsl:apply-templates select="cca2"/>
			</td>
		</tr>
	</xsl:template>
	<xsl:template match="borders">
		<xsl:variable name="codeActuel" select="."/>
		<xsl:value-of select="../../country[cca3=$codeActuel]/name/common"/>
		<xsl:if test="not(position()=last())">
			<xsl:text>, </xsl:text>
		</xsl:if>
	</xsl:template>
	<xsl:template match="latlng">
		<xsl:if test="boolean(following-sibling::latlng)">
		Latitude :
		 <xsl:value-of select="."/>
			<br/> Longitude :
		<xsl:value-of select="following-sibling::latlng"/>
		</xsl:if>
	</xsl:template>
	<xsl:template match="cca2">
		<xsl:variable name="smallcase" select="'abcdefghijklmnopqrstuvwxyz' "/>
		<xsl:variable name="uppercase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ' "/>
		<xsl:variable name="codeCCA2">
			<xsl:value-of select="translate(.,$uppercase,$smallcase)"/>
		</xsl:variable>
		<img src="http://www.geonames.org/flags/x/{$codeCCA2}.gif" alt="" height="40" width="60"/>
	</xsl:template>
</xsl:stylesheet>
